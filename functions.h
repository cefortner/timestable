#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#define MAX_SIZE 100

//Function declarations
bool Is_Hex(char *option, bool *hex);
void Assign_Value(char *value, long int *table_value);
void Check_Values(bool *hex, char *argv, long int *table_value);
void Int_Print(long table_value);
void Hex_Print(long table_value);
int Set_Align(long int table_value);
int Set_Align_Hex(long int table_value);

//Checks argv for -h option, assigns bool if true
bool Is_Hex(char *argv, bool *hex){
    if(strncmp(argv,"-h",strlen(argv)) == 0){
        *hex = true;
        return true;
    }
    else{
        return false;
    }
}

//Ensures Strtol receive valid input, if error, defaults to 10
void Assign_Value(char *value, long int *table_value){

    char *PtrNum = NULL;

    *table_value = strtol(value, &PtrNum, 10);

    if(*table_value > MAX_SIZE){
        printf("I'm sorry, the value you've entered is over my max, defaulting to 10.\n");
        *table_value = 10;
    }
    return;
}

//Handles argv inputs to assign values and check for -h option
void Check_Values(bool *hex, char *argv, long int *table_value){
    if(Is_Hex(argv, hex) == true){
        Assign_Value(argv+3, table_value);
    }
        
    else if(Is_Hex(argv + 3, hex) == true){    
            Assign_Value(argv, table_value);
        }

    else {
        printf("You've requested something that I can't process, I'll do my best.\n");
    }
}

//Set alignment for output length
int Set_Align(long int table_value){
    if(table_value < 10){
        return 2;
    }
    else if(table_value < 31){
        return 3;
    }
    else if(table_value > 31 && table_value < 100){
        return 4;
    }
    else{
        return 5;
    }
}
//Set alignment for output length of hex
int Set_Align_Hex(long int table_value){
    if(table_value < 4){
        return 1;
    }
    else if(table_value < 16){
        return 2;
    }
    else if(table_value < 64){
        return 3;
    }
    else{
        return 4;
    }
}

void Int_Print(long int table_value){
    int align = 0;
    align = Set_Align(table_value);
    printf("%*s",align,"*");
    for(long int i = 1; i != table_value+1; i++){
        printf("\t%*ld",align, i);
    }
    printf("\n");
    for(long int i = 1; i != table_value+1; i++){
        printf("%*ld",align, i);
            for(long int j = 1; j != table_value+1; j++){
                printf("\t%*ld",align, j*i);
            }
            printf("\n");
    }

}

void Hex_Print(long int table_value){   
    int align = 0;
    align = Set_Align_Hex(table_value);
    printf("%*s",align,"*");
    for(unsigned int i = 1; i != (unsigned int) table_value+1; i++){
        printf("\t%*x",align, i);
    }
    printf("\n");
    for(unsigned int i = 1; i != (unsigned int)table_value+1; i++){
        printf("%*x",align, i);
            for(unsigned int j = 1; j != (unsigned int) table_value+1; j++){
                printf("\t%*x",align,j*i);
            }
            printf("\n");
    }

    return;
}
