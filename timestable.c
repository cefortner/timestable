#include "functions.h"



int main(int argc, char *argv[]){
    long int table_value = 10;
    bool hex = false;

    if(argc == 3){
        Check_Values(&hex,argv[1], &table_value);
    }
    
    else if(argc == 2){
        Assign_Value(argv[1], &table_value);
    }

    if(hex == false){
        Int_Print(table_value);
    }

    else{
        Hex_Print(table_value);
    }

return 0;
}